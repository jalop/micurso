---
layout: page
title: Código de Conducta
permalink: /coc/
---
Desde el equipo de coordinación de este grupo de cursos deseamos desarrollar nuestras actividades en el mejor ambiente de trabajo posible, priviliegiando los principios de apertura, calidad y respeto que deben guíar el trabajo de la Universidad Central de Venezuela. 

En ese sentido hemos resumido algunos compromisos mínimos que deben ser compartidos por docentes y estudiantes dentro y fuera de los espacios de aprendizaje.

* El seguimiento de estrictas normas de ética profesional y personal.
* La valoración de la función educativa de calidad de nuestras actividades.
* La responsabilidad y honestidad en el proceso de aprendizaje.
* El trato respetuoso, cortés y considerado a todas las personas.
* El respeto a los diferentes puntos de vista y experiencias.
* El respeto a la privacidad y seguridad de los demás.

* En particular se espera que cada profesor:
	* Tenga en cuenta las necesidades y circunstancias de los estudiantes y adecue las exigencias de las actividades a la cantidad de materia y horas de dedicación del alumno según el plan de trabajo acordado.
	* Proporcione a los mismos información clara y detallada sobre los objetivos del curso y exponga oportunamente los modos y criterios de calificación.
* Se espera que cada estudiante:
	* Ayude a sus compañeros, haciendo que esta sea una comunidad de aprendizaje efectiva.
    * Trabaje individualmente:
        * No plagie.
        * No presente un trabajo que no puede defender.
        * Siempre rinda crédito a las fuentes de información.

## Acción
* Las autoridades académicas de la Facultad de Ciencias de la UCV tienen la obligación de velar por el cumplimiento de toda norma universitaria. Por lo cual deben ser informadas de cualquier actividad que pudiera ser una violación de las mismas.
* Las vías naturales de consulta o denuncia son
	* Docentes de los cursos
	* Representantes estudiantiles y Centros de Estudiantes
	* Consejos de Escuelas y de Facultad
	* Decanato y Coordinaciones de la Facultad de Ciencias

## Fuentes consultadas en la elaboración de este documento:
* Código de Conducta de [LA-CoNGA physics](https://laconga.redclara.net/courses/intro/CodigoConducta.html)
* Student Handbookon Referencing, [Johns Hopkins Bloomberg School of Public Health](https://www.jhsph.edu/academics/degree-programs/master-of-public-health/current-students/JHSPH-StudentReferencing_handbook.pdf)
* Student Code of Conduct, [School of Engineering and Applied Science University of Pennsylvania](https://ugrad.seas.upenn.edu/student-handbook/student-code-of-conduct/)
