---
layout: post
title:  "Plan de actividades semestre 1-2022"
date:   2022-06-10 17:00:00 -0400
categories: planificación
katex: yes
tags:
- Jekyll
- LaTeX
---
# Plan de actividades: 2103 (B,F,M,Q), 2104 (B,Q) 5141 (G)
* **Período lectivo 1-2022**
* **Departamento de Física, Facultad de Ciencias UCV**
* **Libro de referencia: Tipler-Mosca 6ta edición**  
TIPLER, P. A.; MOSCA, G. Física para la Ciencia y la Tecnología vol. 1 6th Edición (Physics for Scientists and Engineers 6th Edition). _Reverté SA, Barcelona_, 2010.

## Cuerpo docente

* Emma Mora
* Ernesto Fuenmayor
* Hely Cordero
* Jackeline Quiñones
* José Antonio López
* José Domingo Mujica

## Temas

### Clave de tema

{:.mbtablestyle}
Número de tema|Tiulo en bibliografía recomendada (Tipler-Mosca 6ta edición)
:---:|:---:
1|Medidas y vectores
2|Movimiento en una dimensón
3|Movimiento en dos y tres dimensiones
4|Leyes de Newton
5|Aplicaciones de las Leyes de Newton
6|Trabajo y energía cinética
7|Conservacion de la energía mecánica
8|Conservacion del momento lineal
9|Rotación
10|Momento angular
13|Fluidos
14|Oscilaciones
15|Ondas viajeras
16|Superposición de ondas y ondas estacionarias
17|Temperatura y teoría cinética de los gases
18|Calor y Primera Ley de la Termodinámica
19|La Segunda ley de la Termodinámica
20|Propiedades y procesos térmicos


### Distribución de temas semanales y docente encargado

{:.mbtablestyle}
|Semana|Fecha incio|Fecha fin|Fis1 BQ|Fis1 FM|Fis1 G|Fis2 BQ|
|:-----:|:-----:|:-----:|:-------|:-------|:-------|:-------:|
|1|2022-06-20|2022-06-24|1 (López)|1 (López)|1 (López)|8 (Fuenmayor)|
|2|2022-06-27|2022-07-01|1 (López)|2 (Cordero)|2 (Cordero)|8 (Fuenmayor)|
|3|2022-07-04|2022-07-08|2 (Cordero)|3 (López)|3 (López)|9 (Mora)|
|4|2022-07-11|2022-07-15|2 (Cordero)|4 (Fuenmayor)|4 (Fuenmayor)|9 (Mora)|
|5|2022-07-18|2022-07-22|3 (López)|5 (Cordero)|5 (Cordero)|10 (Quiñones)|
|6|2022-07-25|2022-07-29|3 (López)|5 (Cordero)|5 (Cordero)|10 (Quiñones)|
|7|2022-08-01|2022-08-05|4 (Fuenmayor)|6 (Cordero)|6 (Cordero)|13 (Quiñones)|
|8|2022-08-08|2022-08-12|4 (Fuenmayor)|6 (Cordero)|7 (López)|13 (Quiñones)|
|Vacaciones colectivas|2022-08-15|2022-08-19||||
|Vacaciones colectivas|2022-08-22|2022-08-26||||
|Vacaciones colectivas|2022-08-29|2022-09-02||||
|Vacaciones colectivas|2022-09-05|2022-09-09||||
|Vacaciones colectivas|2022-09-12|2022-09-16||||
|Vacaciones colectivas|2022-09-19|2022-09-23||||
|9|2022-09-26|2022-09-30|5 (Cordero)|7 (López)|8 (Fuenmayor)|14 (Quiñones)|
|10|2022-10-03|2022-10-07|5 (Cordero)|7 (López)|9 (Mora)|14 (Quiñones)|
|11|2022-10-10|2022-10-14|5 (Cordero)|8 (Fuenmayor)|10 (Quiñones)|15 (Mora)|
|12|2022-10-17|2022-10-21|6 (Cordero)|8 (Fuenmayor)|13 (Quiñones)|16 (Mora)|
|13|2022-10-24|2022-10-28|6 (Cordero)|9 (Mora)|14 (Quiñones)|17 (Fuenmayor)|
|14|2022-10-31|2022-11-04|7 (López)|9 (Mora)|14 (Quiñones)|18 (Mujica)|
|15|2022-11-07|2022-11-11|7 (López)|10 (Quiñones)|15 (Mora)|19 (Mujica)|
|16|2022-11-14|2022-11-18|7 (López)|10 (Quiñones)|16 (Mora)|20 (Mujica)|
|Rep|2022-11-21|2022-11-25|||||

## Evaluación
* La evaluación del curso es mixta. Una parte se realiza de forma continua en el aula virtual (40% del total). Otra parte se realizará en el salón de clase (60% del total)
* Evaluación presencial: constará de 3 exámenes de desarrollo con valor de 20% cada uno.
* Evaluación a distancia
	* Cada tema será evaluado mediante las actividades asignadas a los estudiantes y con una prueba al finalizar el mismo.
	* El peso de cada tema en la calificación final es proporcional al tiempo dedicado, mediante la siguiente fórmula:

$`Nota = \sum_{i=1}^{16} \frac{n_i*s_i}{16}`$


* Observaciones:
	* $`n_i`$ es la nota definitiva del tema $`i`$-ésimo, $`s_i`$ es el número de semanas dedicadas al tema en la programación.
	* Independientemente de cualquier reprogramación de actividades, se usará la programación original para el cálculo de la nota final.

* La evaluación será realizada a distancia.

* Al iniciar cada tema, el docente compartirá los materiales y actividades con el curso e indicará las fechas de correspondientes a la evaluación.

* Los estudiantes deberán estar matriculados en el campus virtual del curso para poder realizar las actividades.

## Fechas clave

|Semana|Fecha|López|Fuenmayor|Cordero|Mora|Quiñones|Mujica|
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1|2022-06-20|1|8| | | |
|2|2022-06-27|1|8|2| | |
|3|2022-07-04|3| |2|9| |
|4|2022-07-11| |4|2|9| |
|5|2022-07-18|3| |5| |10|
|6 (P1)|2022-07-25|3| |5| |10|
|7|2022-08-01| |4|6| |13|
|8|2022-08-08|7|4|6| |13|
|Vacaciones colectivas|2022-08-15||||||
|Vacaciones colectivas|2022-08-22||||||
|Vacaciones colectivas|2022-08-29||||||
|Vacaciones colectivas|2022-09-05||||||
|Vacaciones colectivas|2022-09-12||||||
|Vacaciones colectivas|2022-09-19||||||
|9|2022-09-26|7|8|5| |14|
|10|2022-10-03|7| |5|9|14|
|11 (P2)|2022-10-10| |8|5|15|10|
|12|2022-10-17| |8|6|16|13|
|13|2022-10-24| |17|6|9|14|
|14|2022-10-31|7|| |9|14|18
|15|2022-11-07|7| | |15|10|19
|16 (P3)|2022-11-14|7| ||16|10|20
|Reparación y Calificación|2022-11-21||||||
{:.mbtablestyle}
