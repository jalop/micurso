---
layout: post
title:  "Plan de actividades semestre 1-2023"
date:   2023-01-30 7:00:00 -0400
categories: planificación
katex: yes
tags:
- Jekyll
- LaTeX
---
# Plan de actividades: 2103 (B,F,M,Q), 2104 (B,Q) 5141 (G)
* **Período lectivo 1-2023**
* **Departamento de Física, Facultad de Ciencias UCV**
* **Libro de referencia: Tipler-Mosca 6ta edición**  
TIPLER, P. A.; MOSCA, G. Física para la Ciencia y la Tecnología vol. 1 6th Edición (Physics for Scientists and Engineers 6th Edition). _Reverté SA, Barcelona_, 2010.

## Cuerpo docente

* Coral Campos
* Daniel Brito
* Emma Mora
* Ernesto Fuenmayor
* Hely Cordero
* Jackeline Quiñones
* José Antonio López
* Pío Arias

## Temas

### Clave de tema


Número de tema|Tiulo en bibliografía recomendada (Tipler-Mosca 6ta edición)
:---:|:---:
1|Medidas y vectores
2|Movimiento en una dimensón
3|Movimiento en dos y tres dimensiones
4|Leyes de Newton
5|Aplicaciones de las Leyes de Newton
6|Trabajo y energía cinética
7|Conservacion de la energía mecánica
8|Conservacion del momento lineal
9|Rotación
10|Momento angular
13|Fluidos
14|Oscilaciones
15|Ondas viajeras
16|Superposición de ondas y ondas estacionarias
17|Temperatura y teoría cinética de los gases
18|Calor y Primera Ley de la Termodinámica
19|La Segunda ley de la Termodinámica
20|Propiedades y procesos térmicos
{:.mbtablestyle}

### Distribución de temas semanales y docente encargado


|Semana|Fecha incio|Fecha fin|Fis1 BQ|Fis1 FM|Fis1 G|Fis2 BQ|
|:-----:|:-----:|:-----:|:-------|:-------|:-------|:-------:|
|1|2023-01-30|2023-02-03|1 (López)|1 (López)|1 (López)|8 (Fuenmayor)|
|2|2023-02-06|2023-02-10|1 (López)|2 (Cordero)|2 (Cordero)|8 (Fuenmayor)|
|3|2023-02-13|2023-02-17|2 (Cordero)|3 (Campos)|3 (Campos)|9 (Mora)|
|4|2023-02-20|2023-02-24|2 (Cordero)|4 (Fuenmayor)|4 (Fuenmayor)|9 (Mora)|
|5|2023-02-27|2023-03-03|3 (Campos)|5 (Cordero)|5 (Cordero)|10 (Mora)|
|6|2023-03-06|2023-03-10|3 (Campos)|5 (Cordero)|5 (Cordero)|10 (Mora)|
|7|2023-03-13|2023-03-17|4 (Fuenmayor)|6 (Arias)|6 (Arias)|13 (Quiñones)|
|8|2023-03-20|2023-03-24|4 (Fuenmayor)|6 (Arias)|6 (Arias)|13 (Quiñones)|
|9|2023-03-27|2023-03-31|5 (Brito)|7 (Arias)|7 (Arias)|14 (Quiñones)|
|SS|2023-04-03|2023-04-07|||||
|10|2023-04-10|2023-04-14|5 (Brito)|7 (Arias)|8 (Fuenmayor)|14 (Quiñones)|
|11|2023-04-17|2023-04-21|5 (Brito)|8 (Fuenmayor)|9 (Mora)|15 (Campos)|
|12|2023-04-24|2023-04-28|6 (Arias)|8 (Fuenmayor)|10 (Mora)|16 (Campos)|
|13|2023-05-01|2023-05-05|6 (Arias)|9 (Mora)|13 (Quiñones)|17 (Fuenmayor)|
|SF|2023-05-08|2023-05-12|||||
|14|2023-05-15|2023-05-19|7 (Arias)|9 (Mora)|14 (Quiñones)|18 (Fuenmayor)|
|15|2023-05-22|2023-05-26|7 (Arias)|10 (Mora)|15 (Campos)|19 (Fuenmayor)|
|16|2023-05-29|2023-06-02|7 (Arias)|10 (Mora)|16 (Campos)|20 (Fuenmayor)|
|17|2023-06-05|2023-06-09|||||
|Rep|2023-06-12|2023-06-16|||||
{:.mbtablestyle}

## Evaluación
* La evaluación del curso es mixta. Una parte se realiza de forma continua en el aula virtual (40% del total). Otra parte se realizará en el salón de clase (60% del total)
* Evaluación presencial: constará de 3 exámenes de desarrollo con valor de 20% cada uno.
* Evaluación a distancia
	* Cada tema será evaluado mediante las actividades asignadas a los estudiantes y con una prueba al finalizar el mismo.
	* El peso de cada tema en la calificación final es proporcional al tiempo dedicado, mediante la siguiente fórmula:

$`Nota = \sum_{i=1}^{16} \frac{n_i*s_i}{16}`$


* Observaciones:
	* $`n_i`$ es la nota definitiva del tema $`i`$-ésimo, $`s_i`$ es el número de semanas dedicadas al tema en la programación.
	* Independientemente de cualquier reprogramación de actividades, se usará la programación original para el cálculo de la nota final.

* La evaluación será realizada a distancia.

* Al iniciar cada tema, el docente compartirá los materiales y actividades con el curso e indicará las fechas de correspondientes a la evaluación.

* Los estudiantes deberán estar matriculados en el campus virtual del curso para poder realizar las actividades.

## Fechas clave

|Semana|Fecha|López|Fuenmayor|Cordero|Mora|Quiñones|Campos|Brito|Arias|
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
|1|2023-01-30|1|8| | | ||||
|2|2023-02-06|1|8|2| | ||||
|3|2023-02-13|| |2|9| |3|||
|4|2023-02-20||4|2|9| ||||
|5|2023-02-27|| |5|10||3|||
|6|2023-03-06|| |5|10||3|||
|7 (P1)|2023-03-13||4|| |13|||6|
|8|2023-03-20||4|| |13|||6|
|9|2023-03-27|||||14||5|7|
|SS|2023-04-03|||||||||
|10|2023-04-10||8|||14||5|7|
|11|2023-04-17||8||9||15|5||
|12|2023-04-24||8||10||16||6|
|13 (P2)|2023-05-01||17||9||||6|
|SF|2023-05-08|||| |||||
|14|2023-05-15||18||9||||7|
|15|2023-05-22||19||10||15||7|
|16|2023-05-29||20||10||16||7|
|17 (P3)|2023-06-05|||||||||
|Rep|2023-06-12||| ||||||
{:.mbtablestyle}
