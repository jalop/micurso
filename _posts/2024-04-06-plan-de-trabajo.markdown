---
layout: post
title:  "Plan de actividades semestre 1-2024"
date:   2024-04-06 7:00:00 -0400
categories: planificación
katex: yes
tags:
- Jekyll
- LaTeX
---
# Plan de actividades: 2103 (B,F,M,Q)
* **Período lectivo 1-2024**
* **Departamento de Física, Facultad de Ciencias UCV**
* **Libro de referencia: Tipler-Mosca 6ta edición**  
TIPLER, P. A.; MOSCA, G. Física para la Ciencia y la Tecnología vol. 1 6th Edición (Physics for Scientists and Engineers 6th Edition). _Reverté SA, Barcelona_, 2010.

## Cuerpo docente

* Heidi Martínez
* Hely Cordero
* Jackeline Quiñones
* José Antonio López
* Pío Arias

* Christian Medina
* Jesús Alvarado
* Omar León

## Temas

### Clave de tema


Número de tema|Título en bibliografía recomendada (Tipler-Mosca 6ta edición)
:---:|:---:
1|Medidas, incertidumbre y vectores
2|Movimiento en una dimensón
3|Movimiento en dos y tres dimensiones y graficación
4|Leyes de Newton
5|Aplicaciones de las Leyes de Newton
6|Trabajo y energía cinética
7|Conservacion de la energía mecánica y propagación de errores.
{:.mbtablestyle}

### Distribución de temas semanales y docente encargado de aula virtual


| Actividad Clave |  | Lunes | Martes | Miércoles | Jueves | Viernes | Actividad | Responsables |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Inicio de clase | 1 | 2024-04-15 | 2024-04-16 | 2024-04-17 | 2024-04-18 | 2024-04-19 | Medidas e incertidumbre | JAL |
|  | 2 | 2024-04-22 | 2024-04-23 | 2024-04-24 | 2024-04-25 | 2024-04-26 | Vectores | JAL |
|  | 3 | 2024-04-29 | 2024-04-30 | 2024-05-01 | 2024-05-02 | 2024-05-03 | Mov 1D | HC |
|  | 4 | 2024-05-06 | 2024-05-07 | 2024-05-08 | 2024-05-09 | 2024-05-10 | Mov 1D | HC |
| JIE2024 |  | 2024-05-13 | 2024-05-14 | 2024-05-15 | 2024-05-16 | 2024-05-17 |  |  |
|  | 5 | 2024-05-20 | 2024-05-21 | 2024-05-22 | 2024-05-23 | 2024-05-24 | Mov 2D | JAL |
|  | 6 | 2024-05-27 | 2024-05-28 | 2024-05-29 | 2024-05-30 | 2024-05-31 | Mov 2D + graficación | JAL |
| Parcial 1 (25%) + práctica | 7 | 2024-06-03 | 2024-06-04 | 2024-06-05 | 2024-06-06 | 2024-06-07 | Newton | JQ |
|  | 8 | 2024-06-10 | 2024-06-11 | 2024-06-12 | 2024-06-13 | 2024-06-14 | Newton | JQ |
|  | 9 | 2024-06-17 | 2024-06-18 | 2024-06-19 | 2024-06-20 | 2024-06-21 | Aplicaciones | HM |
| Límite de retiro | 10 | 2024-06-24 | 2024-06-25 | 2024-06-26 | 2024-06-27 | 2024-06-28 | Aplicaciones + ecuaciones + propagación de errores | HM |
| Anuncio de proyecto +Parcial 2 (25%) + práctica | 11 | 2024-07-01 | 2024-07-02 | 2024-07-03 | 2024-07-04 | 2024-07-05 | Trabajo | PA |
|  | 12 | 2024-07-08 | 2024-07-09 | 2024-07-10 | 2024-07-11 | 2024-07-12 | Trabajo | PA |
|  | 13 | 2024-07-15 | 2024-07-16 | 2024-07-17 | 2024-07-18 | 2024-07-19 | Energia | PA |
| Entrega de proyectos (5%) | 14 | 2024-07-22 | 2024-07-23 | 2024-07-24 | 2024-07-25 | 2024-07-26 | Energia | PA |
| Parcial 3 (25%) + práctica | 15 | 2024-07-29 | 2024-07-30 | 2024-07-31 | 2024-08-01 | 2024-08-02 | Repaso |  |
| Reparación y entrega de notas |  | 2024-08-05 | 2024-08-06 | 2024-08-07 | 2024-08-08 | 2024-08-09 |  |  |
{:.mbtablestyle}

## Evaluación
* La evaluación del curso es mixta. Una parte se realiza de forma continua en el aula virtual (25% del total). Otra parte se realizará en el salón de clase (75% del total)
* Evaluación presencial: constará de 3 exámenes de desarrollo con valor de 25% cada uno.
* Evaluación a distancia (20%)
	* Cada tema será evaluado mediante las actividades asignadas a los estudiantes y con una prueba al finalizar el mismo.
	* El peso de cada tema en la calificación final es proporcional al tiempo dedicado, mediante la siguiente fórmula:

$`Nota = \sum_{i=1}^{16} \frac{n_i*s_i}{16}`$


* Observaciones:
	* $`n_i`$ es la nota definitiva del tema $`i`$-ésimo, $`s_i`$ es el número de semanas dedicadas al tema en la programación.
	* Independientemente de cualquier reprogramación de actividades, se usará la programación original para el cálculo de la nota final.

* La evaluación será realizada a distancia.

* Al iniciar cada tema, el docente compartirá los materiales y actividades con el curso e indicará las fechas de correspondientes a la evaluación.

* Evaluación a distancia (5%)
	* Proyecto

* Los estudiantes deberán estar matriculados en el campus virtual del curso para poder realizar las actividades.
