---
layout: post
title:  "Plan de actividades semestre 1-2021"
date:   2021-04-26 19:00:00 -0400
categories: planificación
katex: yes
tags:
- Jekyll
- LaTeX
---
# Plan de actividades: 2103 (B,F,M,Q), 2104 (B,Q) 5141 (G)
* **Período lectivo 1-2021**
* **Departamento de Física, Facultad de Ciencias UCV**
* **Libro de referencia: Tipler-Mosca 6ta edición**  
TIPLER, P. A.; MOSCA, G. Física para la Ciencia y la Tecnología vol. 1 6th Edición (Physics for Scientists and Engineers 6th Edition). _Reverté SA, Barcelona_, 2010.

## Cuerpo docente

* Coral Campos
* Emery Dunia
* Emma Mora
* Ernesto Fuenmayor
* Hely Cordero
* Jackeline Quiñones
* José Antonio López

## Temas

### Clave de tema

Número de tema|Tiulo en bibliografía recomendada (Tipler-Mosca 6ta edición)
:---:|:---:
1|Medidas y vectores
2|Movimiento en una dimensón
3|Movimiento en dos y tres dimensiones
4|Leyes de Newton
5|Aplicaciones de las Leyes de Newton
6|Trabajo y energía cinética
7|Conservacion de la energía mecánica
8|Conservacion del momento lineal
9|Rotacion
10|Momento angular
13|Fluidos
14|Oscilaciones
15|Ondas viajeras
16|Superposición de ondas y ondas estacionarias
17|Temperatura y teoría cinética de los gases
18|Calor y Primera Ley de la Termodinámica
19|La Segunda ley de la Termodinámica
20|Propiedades y procesos térmicos


### Distribución de temas semanales y docente encargado

Semana|Fecha incio|Fecha fin|Fis1 BQ|Fis1 FM|Fis1 G|Fis2 BQ
:---:|:---:|:---:|:---:|:---:|:---:|:---:
1|03/05/21|07/05/21|1 (López)|1 (López)|1 (López)|8 (Fuenmayor)
2|10/05/21|14/05/21|1 (López)|2 (Cordero)|2 (Cordero)|8 (Fuenmayor)
3|17/05/21|21/05/21|2 (Cordero)|3 (López)|3 (López)|9 (Mora)
4|24/05/21|28/05/21|2 (Cordero)|4 (Fuenmayor)|4 (Fuenmayor)|9 (Mora)
5|31/05/21|04/06/21|3 (López)|5 (Cordero)|5 (Cordero)|10 (Quiñones)
6|07/06/21|11/06/21|3 (López)|5 (Cordero)|5 (Cordero)|10 (Quiñones)
7|14/06/21|18/06/21|4 (Fuenmayor)|6 (Cordero)|6 (Cordero)|13 (Quiñones)
8|21/06/21|25/06/21|4 (Fuenmayor)|6 (Cordero)|7 (López)|13 (Quiñones)
9|28/06/21|02/07/21|5 (Cordero)|7 (López)|8 (Fuenmayor)|14 (Campos)
10|05/07/21|09/07/21|5 (Cordero)|7 (López)|9 (Mora)|14 (Campos)
11|12/07/21|16/07/21|5 (Cordero)|8 (Fuenmayor)|10 (Quiñones)|15 (Mora)
12|19/07/21|23/07/21|6 (Cordero)|8 (Fuenmayor)|13 (Quiñones)|16 (Mora)
13|26/07/21|30/07/21|6 (Cordero)|9 (Mora)|14 (Campos)|17 (Fuenmayor)
14|02/08/21|06/08/21|7 (López)|9 (Mora)|14 (Campos)|18 (Dunia)
15|09/08/21|13/08/21|7 (López)|10 (Quiñones)|15 (Mora)|19 (Dunia)
Vacaciones|16/08/21|24/09/21||||
16|27/09/21|01/10/21|7 (López)|10 (Quiñones)|16 (Mora)|20 (Dunia)
17|04/10/21|08/10/21|7 (López)|10 (Quiñones)|16 (Mora)|20 (Dunia)
Reparación y calificación|11/10/21|15/10/21||||

## Evaluación
* La evaluación del curso es continua
	* Cada tema será evaluado mediante las actividades asignadas a los estudiantes y con una prueba al finalizar el mismo.
	* El peso de cada tema en la calificación final es proporcional al tiempo dedicado, mediante la siguiente fórmula:

$`Nota = \sum_{i=1}^{16} \frac{n_i*s_i}{16}`$


* Observaciones:
	* $`n_i`$ es la nota definitiva del tema $`i`$-ésimo, $`s_i`$ es el número de semanas dedicadas al tema en la programación.
	* Independientemente de cualquier reprogramación de actividades, se usará la programación original para el cálculo de la nota final.

* La evaluación será realizada a distancia.

* Al iniciar cada tema, el docente compartirá los materiales y actividades con el curso e indicará las fechas de correspondientes a la evaluación.

* Los estudiantes deberán estar matriculados en el campus virtual del curso para poder realizar las actividades.

## Fechas clave

|Fecha|López|Cordero|Fuenmayor|Quiñones|Campos|Mora|Dunia|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|03/05/21|1||8||||
|10/05/21|1|2|8||||
|17/05/21|3|2||||9|
|24/05/21||2|4|||9|
|31/05/21|3|5||10|||
|07/06/21|3|5||10|||
|14/06/21||6|4|13|||
|21/06/21|7|6|4|13|||
|28/06/21|7|5|8||14||
|05/07/21|7|5|||14|9|
|12/07/21||5|8|10||15|
|19/07/21||6|8|13||16|
|26/07/21||6|17||14|9|
|02/08/21|7||||14|9|18
|09/08/21|7|||10||15|19
|16/08/21- 24/09/21|||||||
|27/09/21|7|||10||16|20
|04/10/21|7|||10||16|20
|11/10/21|Reparación||||||
