---
layout: post
title:  "Plan de actividades semestre 2-2023"
date:   2023-10-16 7:00:00 -0400
categories: planificación
katex: yes
tags:
- Jekyll
- LaTeX
---
# Plan de actividades: 2103 (B,F,M,Q)
* **Período lectivo 2-2023**
* **Departamento de Física, Facultad de Ciencias UCV**
* **Libro de referencia: Tipler-Mosca 6ta edición**  
TIPLER, P. A.; MOSCA, G. Física para la Ciencia y la Tecnología vol. 1 6th Edición (Physics for Scientists and Engineers 6th Edition). _Reverté SA, Barcelona_, 2010.

## Cuerpo docente

* Ernesto Fuenmayor
* Hely Cordero
* José Antonio López
* Pío Arias

## Temas

### Clave de tema


Número de tema|Título en bibliografía recomendada (Tipler-Mosca 6ta edición)
:---:|:---:
1|Medidas, incertidumbre y vectores
2|Movimiento en una dimensón
3|Movimiento en dos y tres dimensiones y graficación
4|Leyes de Newton
5|Aplicaciones de las Leyes de Newton
6|Trabajo y energía cinética
7|Conservacion de la energía mecánica y propagación de errores.
{:.mbtablestyle}

### Distribución de temas semanales y docente encargado


| Actividad Clave |  | Lunes | Martes | Miércoles | Jueves | Viernes | Actividad | Responsables |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Inicio de clase | 1 | 2023-10-16 | 2023-10-17 | 2023-10-18 | 2023-10-19 | 2023-10-20 | Bienvenida + Medidas | Todo el grupo |
|  | 2 | 2023-10-23 | 2023-10-24 | 2023-10-25 | 2023-10-26 | 2023-10-27 | Medidas 2 e Incertidumbre | JAL |
|  | 3 | 2023-10-30 | 2023-10-31 | 2023-11-01 | 2023-11-02 | 2023-11-03 | Vectores | JAL |
|  | 4 | 2023-11-06 | 2023-11-07 | 2023-11-08 | 2023-11-09 | 2023-11-10 | Mov 1D | HC |
|  | 5 | 2023-11-13 | 2023-11-14 | 2023-11-15 | 2023-11-16 | 2023-11-17 | Mov 1D | HC |
|  | 6 | 2023-11-20 | 2023-11-21 | 2023-11-22 | 2023-11-23 | 2023-11-24 | Mov 2D | JAL |
|  | 7 | 2023-11-27 | 2023-11-28 | 2023-11-29 | 2023-11-30 | 2023-12-01 | Mov 2D + graficación | JAL |
| Parcial 1 25% | 8 | 2023-12-04 | 2023-12-05 | 2023-12-06 | 2023-12-07 | 2023-12-08 | Newton | EF |
|  | 9 | 2023-12-11 | 2023-12-12 | 2023-12-13 | 2023-12-14 | 2023-12-15 | Newton | EF |
| Asueto Navideño |  | 2023-12-18 | 2023-12-19 | 2023-12-20 | 2023-12-21 | 2023-12-22 |  |  |
| Asueto Navideño |  | 2023-12-25 | 2023-12-26 | 2023-12-27 | 2023-12-28 | 2023-12-29 |  |  |
| Asueto Navideño |  | 2024-01-01 | 2024-01-02 | 2024-01-03 | 2024-01-04 | 2024-01-05 |  |  |
| Última semana de retiro | 10 | 2024-01-08 | 2024-01-09 | 2024-01-10 | 2024-01-11 | 2024-01-12 | Aplicaciones | HC |
|  | 11 | 2024-01-15 | 2024-01-16 | 2024-01-17 | 2024-01-18 | 2024-01-19 | Aplicaciones + ecuaciones + propagación de errores | HC |
| Anuncio de proyecto +Parcial 2 25% | 12 | 2024-01-22 | 2024-01-23 | 2024-01-24 | 2024-01-25 | 2024-01-26 | Trabajo | PA |
|  | 13 | 2024-01-29 | 2024-01-30 | 2024-01-31 | 2024-02-01 | 2024-02-02 | Trabajo | PA |
|  | 14 | 2024-02-05 | 2024-02-06 | 2024-02-07 | 2024-02-08 | 2024-02-09 | Energia | PA |
| Carnaval | 15 | 2024-02-12 | 2024-02-13 | 2024-02-14 | 2024-02-15 | 2024-02-16 | Energia | PA |
| Entrega de proyectos 5% | 16 | 2024-02-19 | 2024-02-20 | 2024-02-21 | 2024-02-22 | 2024-02-23 | Repaso. | PA |
| Parcial 3 25% |  | 2024-02-26 | 2024-02-27 | 2024-02-28 | 2024-02-29 | 2024-03-01 |  |  |
| Reparaciones |  | 2024-03-04 | 2024-03-05 | 2024-03-06 | 2024-03-07 | 2024-03-08 |  |  |
{:.mbtablestyle}

## Evaluación
* La evaluación del curso es mixta. Una parte se realiza de forma continua en el aula virtual (25% del total). Otra parte se realizará en el salón de clase (75% del total)
* Evaluación presencial: constará de 3 exámenes de desarrollo con valor de 25% cada uno.
* Evaluación a distancia (20%)
	* Cada tema será evaluado mediante las actividades asignadas a los estudiantes y con una prueba al finalizar el mismo.
	* El peso de cada tema en la calificación final es proporcional al tiempo dedicado, mediante la siguiente fórmula:

$`Nota = \sum_{i=1}^{16} \frac{n_i*s_i}{16}`$


* Observaciones:
	* $`n_i`$ es la nota definitiva del tema $`i`$-ésimo, $`s_i`$ es el número de semanas dedicadas al tema en la programación.
	* Independientemente de cualquier reprogramación de actividades, se usará la programación original para el cálculo de la nota final.

* La evaluación será realizada a distancia.

* Al iniciar cada tema, el docente compartirá los materiales y actividades con el curso e indicará las fechas de correspondientes a la evaluación.

* Evaluación a distancia (5%)
	* Proyecto

* Los estudiantes deberán estar matriculados en el campus virtual del curso para poder realizar las actividades.
