---
layout: page
title: Cursos
permalink: /cursos/
---
Información general dedicada al curso:
* (2103) Física General 1
	* Licenciaturas de Biología, Física, Matemática y Química [Canal Telegram](https://t.me/+LN83D0kEL7ViNTEx)

![qr-telegram](images/qrTelegram2103.png)

<!--
Información general dedicada a los cursos:
* (2103) Física General 1
	* Licenciaturas de Biología y Química [Canal Telegram](https://t.me/joinchat/NDmjJcnGsiI0OWU5)
	* Licenciaturas de Física y Matemática [Canal Telegram](https://t.me/joinchat/zitVzqpXB7c1NmRh)
* (5141) Física 1
	* Licenciatura de Geoquímica [Canal Telegram](https://t.me/joinchat/gQFR4xPXNhE4NThh)
* (2104) Física General 2
	* Licenciaturas de Biología y Química [Canal Telegram](https://t.me/joinchat/IrMzoCoivgFiMWQx)
-->
